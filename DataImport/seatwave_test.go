// seatwave_test.go
package dtimp

import (
	"testing"
)

func TestDateParse(t *testing.T) {

	pErr, day, localTime, _ := parseDate("/Date(1421436600000+0000)/")

	if pErr != nil {
		t.Error("Seatwave Date Parse Error")
	}
	if day != "2015-01-16" {
		t.Error("Date incorrectly parsed " + day)
	}
	if localTime != "19:30" {
		t.Error("Time incorrectly parsed " + localTime)
	}

	pErr, day, localTime, _ = parseDate("/Date(1431194400000+0100)/")

	if pErr != nil {
		t.Error("Seatwave Date Parse Error")
	}
	if day != "2015-05-09" {
		t.Error("Date incorrectly parsed " + day)
	}
	if localTime != "19:00" {
		t.Error("Time incorrectly parsed " + localTime)
	}

	pErr, day, localTime, _ = parseDate("/Date(1431194400000+0130)/")

	if pErr != nil {
		t.Error("Seatwave Date Parse Error")
	}
	if day != "2015-05-09" {
		t.Error("Date incorrectly parsed " + day)
	}
	if localTime != "19:30" {
		t.Error("Time incorrectly parsed " + localTime)
	}

}

func TestSeatwaveImport(t *testing.T) {

	note := make(chan []Event)

	ImportData(note)

	received := <-note

	ev := received[0]

	if len(ev.Date) == 0 {
		t.Error("Missing Date", ev)
	}
	if len(ev.LocalTime) == 0 {
		t.Error("Missing Local time", ev)
	}
	if len(ev.Name) == 0 {
		t.Error("Missing Event name", ev)
	}
	if len(ev.PerformerId) == 0 {
		t.Error("Missing Performer Id", ev)
	}
	if len(ev.PerformerName) == 0 {
		t.Error("Missing Performer Name", ev)
	}
	if len(ev.VenueId) == 0 {
		t.Error("Missing Venue Id", ev)
	}
	if len(ev.VenueName) == 0 {
		t.Error("Missing Venue Name", ev)
	}
	if len(ev.TicketUrl) == 0 {
		t.Error("Missing Ticket Url", ev)
	}

}
