// entityWriter_test.go
package dtimp

import (
	"Timbre/DataSource"
	_ "fmt"
	"testing"
)

func TestWriteEvents(t *testing.T) {

	ev := Event{}
	ev.Name = "Foo Fighters"
	ev.VenueId = "sw:venue:2343"
	ev.VenueName = "O2"
	ev.PerformerId = "sw:performer:2343"
	ev.PerformerName = "Foo Fighters"
	ev.id = "sw:event:1232"
	err := WriteEntities([]Replaceable{ev}, FilterForBucket(dtsrc.ImportBucket()), WriterForBucket(dtsrc.ImportBucket()))

	if err != nil {
		t.Error(err.Error())
	}

}
