// structures.go
package dtimp

import (
	"encoding/json"
)

type Replaceable interface {
	ShouldReplace(existing Replaceable) bool
	Id() string
	Json() (data []byte, err error)
}

type Event struct {
	Name          string `json:"name"`
	Date          string `json:"date"`
	DayId         int    `json:"dayId"`
	LocalTime     string `json:"localTime"`
	PerformerId   string `json:"performerId"`
	PerformerName string `json:"performerName"`
	VenueId       string `json:"venueId"`
	VenueName     string `json:"venueName"`
	TicketUrl     string `json:"ticketUrl,omitempty"`
	id            string `json:"-"`
	Source        string `json:"source"`
}

func (self Event) ShouldReplace(existing Replaceable) bool {

	if existing == nil {
		return true
	}
	ev, ok := existing.(Event)
	if !ok {
		return false
	}

	if self.id != ev.id {
		return false
	}
	return true
}

func (self Event) Id() string {
	return self.id
}

func (self *Event) SetId(id string) {
	self.id = id
}

func (self Event) Json() (data []byte, err error) {
	data, err = json.Marshal(self)
	return
}
