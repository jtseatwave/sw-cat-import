// entityWriter.go
package dtimp

import (
	"encoding/json"
	"fmt"
	"github.com/couchbase/gomemcached"
	"github.com/couchbaselabs/go-couchbase"
)

func main() {
	fmt.Println("Hello World!")
}

type EntitiesFilterFunc func(entities []Replaceable) (filtered []Replaceable, err error)

type EntityWriterFunc func(r Replaceable) (err error)

func WriteEntities(entities []Replaceable, ffn EntitiesFilterFunc, wfn EntityWriterFunc) error {

	f, err := ffn(entities)
	if err != nil {
		return err
	}
	for _, ent := range f {
		if er := wfn(ent); er != nil {
			err = er
		}
	}
	return err
}

func FilterForBucket(b *couchbase.Bucket) (fn EntitiesFilterFunc) {

	fn = func(entities []Replaceable) (filtered []Replaceable, err error) {

		eById, ids := createEntityMap(entities)
		var res map[string]*gomemcached.MCResponse
		if res, err = b.GetBulk(ids); err != nil {

			return
		}
		if len(res) == 0 {
			filtered = entities
			return
		}

		oldEnts := make(map[string]Replaceable)
		var k string
		var resp *gomemcached.MCResponse
		for k, resp = range res {
			var r Replaceable
			r, err = Loader(resp)
			if err != nil {
				continue
			}
			oldEnts[k] = r

		}

		for k, r := range eById {

			if r.ShouldReplace(oldEnts[k]) {
				filtered = append(filtered, r)
			}

		}

		return

	}

	return

}

func createEntityMap(ents []Replaceable) (eById map[string]Replaceable, ids []string) {

	eById = make(map[string]Replaceable)
	for _, ev := range ents {
		eById[ev.Id()] = ev
		ids = append(ids, ev.Id())
	}

	return

}

func Loader(r *gomemcached.MCResponse) (rep Replaceable, err error) {

	js := make(map[string]interface{})
	if err = json.Unmarshal(r.Body, &js); err != nil {
		return
	}

	t, ok := js["type"].(string)

	if ok {
		switch t {

		case "event":
			rep = Event{}
			json.Unmarshal(r.Body, &rep)
			return
		}
	}

	return
}

func WriterForBucket(b *couchbase.Bucket) (fn EntityWriterFunc) {

	fn = func(r Replaceable) (err error) {
		err = b.WriteUpdate(r.Id(), 0, func(current []byte) (updated []byte, opt couchbase.WriteOptions, err error) {
			updated, err = r.Json()
			opt = couchbase.Raw
			return
		})
		return
	}
	return

}
