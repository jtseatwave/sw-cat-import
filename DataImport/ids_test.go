// ids_test.go
package dtimp

import (
	"strconv"
	"testing"
)

func TestCreateEventId(t *testing.T) {

	s := CreateEventId(Seatwave, strconv.Itoa(50))
	if s != "sw:event:50" {
		t.Error("Event id should be sw:event:50", s)
	}

}
