// seatwave.go
package dtimp

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"sync"
	"time"
)

type seatwave_Response struct {
	Paging seatwave_Paging  `json: "Paging"`
	Events []seatwave_Event `json: "Events"`
}

type seatwave_Paging struct {
	PageNumber       int
	PageSize         int
	PageResultCount  int
	TotalResultCount int
	TotalPageCount   int
}

type seatwave_Event struct {
	Id          int    `json:"Id"`
	Date        string `json:"Date"`
	Name        string `json:"EventGroupName"`
	VenueName   string `json:"VenueName"`
	Country     string `json:"Country"`
	TicketUrl   string `json:"SwURL"`
	PerformerId int    `json:"EventGroupId"`
	VenueId     int    `json:"VenueId"`
}

func ImportData(note chan []Event) {

	wg := new(sync.WaitGroup)

	wg.Add(1)
	ch := make(chan []seatwave_Event)

	go importPage(ch, wg, 1)

	go receiveEvents(ch, note)

	go func(wg *sync.WaitGroup, reqCh chan []Event) {
		wg.Wait()
		close(reqCh)
	}(wg, note)

}

func importPage(ch chan []seatwave_Event, waitG *sync.WaitGroup, pageNo int) {

	url := createUrl(pageNo)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)

	defer res.Body.Close()
	defer waitG.Done()

	body, err := ioutil.ReadAll(res.Body)

	data := seatwave_Response{}
	err = json.Unmarshal(body, &data)

	if err != nil {
		fmt.Print(err)
		return
	}

	if shouldRequestNextPage(data.Paging) {
		waitG.Add(1)
		go importPage(ch, waitG, pageNo+1)
	}

	ch <- data.Events

}

func shouldRequestNextPage(paging seatwave_Paging) bool {
	return paging.TotalResultCount > paging.PageNumber*paging.PageSize
}

func createUrl(pageNo int) (url string) {

	url = "http://api.seatwave.com/v2/discovery/events?apikey=b037c81b7fad4547af11f5f65ab8a8ee&pgsize=1000&where=uk&what=music&eventsWithoutTix=true&pgnumber=" + strconv.Itoa(pageNo)
	return
}

func receiveEvents(ch chan []seatwave_Event, eventCh chan []Event) {

	for events := range ch {
		eventCh <- parseEvents(events)
	}

}

func parseEvents(swEvents []seatwave_Event) (events []Event) {

	events = make([]Event, len(swEvents))
	j := 0
	for _, ev := range swEvents {
		err, pEv := convertSeatwaveEvent(ev)
		if err != nil {
			continue
		}
		events[j] = pEv
		j = j + 1
	}
	return
}

func convertSeatwaveEvent(swEvent seatwave_Event) (err error, event Event) {

	event = Event{}
	event.Date = swEvent.Date
	event.Name = swEvent.Name
	event.PerformerId = CreatePerformerId(Seatwave, strconv.Itoa(swEvent.PerformerId))
	event.PerformerName = swEvent.Name
	event.TicketUrl = swEvent.TicketUrl
	event.VenueId = CreateVenueId(Seatwave, strconv.Itoa(swEvent.VenueId))
	event.VenueName = swEvent.VenueName
	event.SetId(CreateEventId(Seatwave, strconv.Itoa(swEvent.Id)))

	pErr, day, localTime, dayId := parseDate(swEvent.Date)

	if pErr != nil {
		err = pErr
		return
	}

	event.Date = day
	event.LocalTime = localTime
	event.DayId = dayId
	return
}

func parseDate(date string) (err error, day string, localTime string, dayId int) {

	re := regexp.MustCompile(`/Date\((\d+)\+(\d{2})(\d{2})`)
	s := re.FindStringSubmatch(date)
	if len(s) <= 3 {
		err = errors.New("Invalid date Format: " + date)
		return
	}
	milliSeconds := s[1]
	hourOffset := s[2]
	minOffset := s[3]

	var f float64
	f, err = strconv.ParseFloat(milliSeconds, 64)
	if err != nil {
		return
	}

	var h int64
	h, err = strconv.ParseInt(hourOffset, 10, 64)
	if err != nil {
		return
	}

	var m int64
	m, err = strconv.ParseInt(minOffset, 10, 64)
	if err != nil {
		return
	}

	secondsInt := int64(f/1000) + 3600*h + 60*m

	uTime := time.Unix(secondsInt, 0).In(time.UTC)

	day = fmt.Sprintf("%d-%02d-%02d", uTime.Year(), uTime.Month(), uTime.Day())
	localTime = fmt.Sprintf("%02d:%02d", uTime.Hour(), uTime.Minute())
	dayId = uTime.Year()*10000 + int(uTime.Month())*100 + uTime.Day()

	return
}
