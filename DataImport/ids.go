// ids.go
package dtimp

type DataSource int

const (
	Seatwave DataSource = iota
	TM       DataSource = iota
)

func sourceString(source DataSource) string {
	switch source {
	case Seatwave:
		return "sw"
	case TM:
		return "tm"
	}
	return ""
}

func SourceId(s string) DataSource {
	switch s {
	case sourceString(Seatwave):
		return Seatwave
	}
	return 0
}

func CreateEventId(source DataSource, sourceId string) string {
	return CreateId(source, sourceId, "event")
}

func CreateVenueId(source DataSource, sourceId string) string {
	return CreateId(source, sourceId, "venue")
}

func CreatePerformerId(source DataSource, sourceId string) string {
	return CreateId(source, sourceId, "performer")
}

func CreateId(source DataSource, sourceId string, typeStr string) string {
	return sourceString(source) + ":" + typeStr + ":" + sourceId
}
