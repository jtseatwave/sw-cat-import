// main.go
package main

import (
	"Timbre/DataImport"
	"Timbre/DataSource"
	"bufio"
	"fmt"
	"os"
)

func main() {
	fmt.Println("Hello World!")
	importSeatwave()
}

func importSeatwave() {

	ch := make(chan []dtimp.Event)

	dtimp.ImportData(ch)

	for events := range ch {

		repl := make([]dtimp.Replaceable, len(events))
		for i, ev := range events {

			repl[i] = ev

		}

		go func() {
			err := dtimp.WriteEntities(repl, dtimp.FilterForBucket(dtsrc.ImportBucket()), dtimp.WriterForBucket(dtsrc.ImportBucket()))

			if err != nil {
				fmt.Println(err.Error())
			}

		}()

	}

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter text: ")
	_, _ = reader.ReadString('\n')

}
