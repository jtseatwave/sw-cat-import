#!/bin/bash

outfile=gotest.out

go test -v ./... | tee $outfile
$GOPATH/bin/go2xunit -fail -input $outfile -output tests.xml

