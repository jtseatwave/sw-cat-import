FROM google/golang


ADD . /gopath/src/Timbre
VOLUME ["/gopath/src/Timbre"]

RUN go get bitbucket.org/tebeka/go2xunit
RUN go get github.com/couchbaselabs/go-couchbase
ENV SCRIPT_PATH /gopath/src/Timbre
WORKDIR /gopath/src/Timbre
CMD ["./run.sh"]
