package dtsrc

import (
	"github.com/couchbaselabs/go-couchbase"
	"log"
	"os"
	"sync"
)

func ImportBucket() *couchbase.Bucket {

	setupBuckets()
	return impBkt

}

func CatalogueBucket() *couchbase.Bucket {
	setupBuckets()
	return catBkt
}

func setupBuckets() {
	var once sync.Once
	once.Do(func() { openConnection(dataBaseUrl()) })
}

var impBkt *couchbase.Bucket
var catBkt *couchbase.Bucket

var cb couchbase.Client

func dataBaseUrl() (url string) {
	url = os.Getenv("CB_URL")
	if url == "" {
		url = "http://localhost:8091/"
	} else {
		url = "http://" + url
	}
	return
}

func openConnection(url string) {

	c, err := couchbase.Connect(url)
	if err != nil {
		log.Fatalf("Error connecting:  %v", err)
	}

	cb = c

	pool, err := c.GetPool("default")

	if err != nil {
		log.Fatalf("Error getting pool:  %v", err)
	}

	bucket, err := pool.GetBucket("TimbreImport")
	if err != nil {
		log.Fatalf("Error getting bucket:  %v", err)
	}

	impBkt = bucket

	bucket, err = pool.GetBucket("TimbreImport")
	if err != nil {
		log.Fatalf("Error getting bucket:  %v", err)
	}

	catBkt = bucket

}
